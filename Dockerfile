FROM node:12.18-alpine as base
WORKDIR /app
ARG env=prod

COPY . .
RUN npm install && npm run build:prod

FROM nginx:alpine
COPY --from=base /app/dist/pokedex /usr/share/nginx/html
EXPOSE 80
EXPOSE 443
