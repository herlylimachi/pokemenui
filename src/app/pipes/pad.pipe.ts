import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pad',
})
export class PadPipe implements PipeTransform {
  transform(value: number, size: number): string {
    if (value == null) {
      return '';
    }
    let s = value + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  }
}
