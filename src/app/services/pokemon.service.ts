import { Ipokemon } from './../interfaces/ipokemon';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  uriBase: string = environment.apiUriPokemones + '/Pokemon';
  //uriBase: string = 'https://raw.githubusercontent.com/fanzeyi/pokemon.json/master/pokedex.json';
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<Ipokemon[]> {
    return this.httpClient.get<Ipokemon[]>(this.uriBase);
  }

  get(id: number): Observable<Ipokemon> {
    const uri = this.uriBase + '/' + id;
    return this.httpClient.get<Ipokemon>(uri);
  }
}
