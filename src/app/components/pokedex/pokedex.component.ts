import { Ipokemon } from './../../interfaces/ipokemon';
import { PokemonService } from './../../services/pokemon.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.css'],
})
export class PokedexComponent implements OnInit {
  pokemones: Ipokemon[] = [];
  formBusqueda: FormGroup;
  constructor(private pokemonService: PokemonService, private fb: FormBuilder) {
    this.formBusqueda = this.fb.group({
      nombreNumero: [''],
    });
  }

  ngOnInit(): void {
    this.pokemonService.getAll().subscribe((value) => {
      this.pokemones = value.slice(0, 16);
    });
  }

  buscarPokemon(): void {
    const itemSearch = this.formBusqueda.value.nombreNumero as string;
    this.pokemonService.getAll().subscribe((pPokemones) => {
      this.pokemones = pPokemones.filter((pPokemon) => {
        return (
          pPokemon.id.toString().includes(itemSearch.toLowerCase()) ||
          pPokemon.name.english.toLowerCase().includes(itemSearch.toLowerCase())
        );
      });
    });
  }
}
