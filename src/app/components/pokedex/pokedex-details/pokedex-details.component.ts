import { Ipokemon } from './../../../interfaces/ipokemon';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon.service';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-pokedex-details',
  templateUrl: './pokedex-details.component.html',
  styleUrls: ['./pokedex-details.component.css'],
})
export class PokedexDetailsComponent implements OnInit {
  id: number;
  pokemones: Ipokemon[] = [];
  pokemon: Ipokemon;
  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((value) => {
      if (value.id != null) {
        this.id = value.id;
        this.Load();
      }
    });
  }

  // Load() {
  //   this.pokemonService.get(this.id).subscribe((value) => {
  //     this.pokemon = value.find((value2) => {
  //       return value2.id == this.id;
  //     });
  //   });
  // }
  Load() {
    this.pokemonService.get(this.id).subscribe((value) => {
      this.pokemon = value;
      console.log(this.pokemon);
    });
  }
}
