import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { AplicacionesComponent } from './components/aplicaciones/aplicaciones.component';
import { JuegoDeCartasComponent } from './components/juego-de-cartas/juego-de-cartas.component';
import { TvpokemonComponent } from './components/tvpokemon/tvpokemon.component';
import { PlayPokemonComponent } from './components/play-pokemon/play-pokemon.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PokedexDetailsComponent } from './components/pokedex/pokedex-details/pokedex-details.component';

import { HttpClientModule } from '@angular/common/http';
import { PadPipe } from './pipes/pad.pipe';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PokedexComponent,
    InicioComponent,
    AplicacionesComponent,
    JuegoDeCartasComponent,
    TvpokemonComponent,
    PlayPokemonComponent,
    NotFoundComponent,
    PadPipe,
    PokedexDetailsComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
