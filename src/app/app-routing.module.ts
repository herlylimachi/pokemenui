import { NotFoundComponent } from './components/not-found/not-found.component';
import { PlayPokemonComponent } from './components/play-pokemon/play-pokemon.component';
import { TvpokemonComponent } from './components/tvpokemon/tvpokemon.component';
import { JuegoDeCartasComponent } from './components/juego-de-cartas/juego-de-cartas.component';
import { AplicacionesComponent } from './components/aplicaciones/aplicaciones.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { PokedexDetailsComponent } from './components/pokedex/pokedex-details/pokedex-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'Pokedex', component: PokedexComponent },
  { path: 'Aplicaciones', component: AplicacionesComponent },
  { path: 'JuegoDeCartas', component: JuegoDeCartasComponent },
  { path: 'TvPokemon', component: TvpokemonComponent },
  { path: 'PlayPokemon', component: PlayPokemonComponent },
  { path: 'PokedexDetails/:id', component: PokedexDetailsComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
