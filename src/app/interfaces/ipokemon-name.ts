export interface IpokemonName {
  english: string;
  japanese: string;
  chinese: string;
  french: string;
}
