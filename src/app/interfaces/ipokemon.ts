import { IpokemonName } from './ipokemon-name';
import { Ipokemonbase } from './ipokemonbase';
export interface Ipokemon {
  id: number;
  name: IpokemonName;
  type: string[];
  base: Ipokemonbase;
}
